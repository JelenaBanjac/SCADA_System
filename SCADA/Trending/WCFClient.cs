﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.ServiceModel;
using WCFCommon;

namespace Trending
{
    static class WCFClient
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Thread.Sleep(1000);

            Uri address = new Uri("net.tcp://localhost:8080/ITrending");
            NetTcpBinding binding = new NetTcpBinding();

            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<ITrending> factory = new ChannelFactory<ITrending>(binding, new EndpointAddress(address));
            ITrending proxy = factory.CreateChannel();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TrendingForm(proxy));
        }
    }
}
