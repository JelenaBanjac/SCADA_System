﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{

    [DataContract]
    public class DigitalOutput : Output
    {
        public DigitalOutput() { }

        public DigitalOutput(string tagId, string description, string IOaddress, double initialValue)
            : base(tagId, description, IOaddress, initialValue)
        { }


        public override string ToString()
        {
            return "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
                + " Tag ID: " + this.TagId + " Initial value: " + this.initialValue + " Description: " + this.Description + "\n I/O Address: " + this.IOAddress
                + "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        }
    }
}
