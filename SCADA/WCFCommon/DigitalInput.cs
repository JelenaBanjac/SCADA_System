﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    public class DigitalInput : Input
    {
        public DigitalInput() { }

        public DigitalInput(string tagName, string description, string IOaddress, int scanTime, AlarmBlock alarm = null, bool onOffScan = true, bool autoManual = true) : base(tagName, description, IOaddress, scanTime, alarm, onOffScan, autoManual)
        {

        }

        public override string ToString()
        {

            string ret = "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" + "Tag ID: " + this.TagId + " Value: " + this.Driver.Value + " Description: " + this.Description + "\nI/Oaddress: " + this.IOAddress + " Scan time: " + this.scanTime + " On/off scan: " + this.onOffScan + "\n Auto/manual: " + this.autoManual;
            if (this.alarm == null)
            {
                return ret + " NO alarm \n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
            }
            return ret + "\n Alarm type: " + ((DigitalAlarmBlock)(this.alarm)).AlarmType + "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        }

    }
    
}
