﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    
    [DataContract]
    public class AnalogInput : Input
    {

        [DataMember]
        public double lowLimit { get; set; }

        [DataMember]
        public double highLimit { get; set; }

        [DataMember]
        public string units { get; set; }

        public AnalogInput() { }

        public AnalogInput(string tagName, string description, string IOaddress, int scanTime, double lowLimit, double highLimit, double ampl, string units, AlarmBlock alarm = null, bool onOffScan = true, bool autoManual = true)
            : base(tagName, description, IOaddress, scanTime, alarm, onOffScan, autoManual)
        {
            this.lowLimit = lowLimit;
            this.highLimit = highLimit;
            this.units = units;
            this.Driver.amplitude = ampl;
        }

        public override string ToString()
        {
           
            string ret = "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" + "Tag ID: " + this.TagId + " Value: " + this.Driver.Value + " Description: " + this.Description + "\nI/Oaddress: " + this.IOAddress + " Scan time: " + this.scanTime + " On/off scan: " + this.onOffScan + "\n Auto/manual: " + this.autoManual;
            if (this.alarm == null)
            {
                return ret + " NO alarm \n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
            }
            return ret + "\n Alarm low limit: " + ((AnalogAlarmBlock)(this.alarm)).Low + " Alarm high limit: " + ((AnalogAlarmBlock)(this.alarm)).High + "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        }
    }
}
