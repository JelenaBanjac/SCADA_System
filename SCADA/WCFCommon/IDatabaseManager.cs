﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFCommon
{
    [ServiceContract]
    public interface IDatabaseManager
    {
        /* Add new tag value to server. */
        [OperationContract]
        void AddTag(Tag tag);

        /* Remove tag value from the server. */
        [OperationContract]
        void RemoveTag(string tagId);

        /* Scanning tag value for console printing - ON/OFF. */
        [OperationContract]
        void ToggleTagOnOff(string tagId);

        /* Scanning tag values from Simulation Driver - AUTO/MANUAL. */
        [OperationContract]
        void ToggleTagAutoManual(string tagId);

        [OperationContract]
        void StopDisplayThreads();

        /* Set initial value to tag. */
        [OperationContract]
        void SetInitialValue(string tagId, double value);

        [OperationContract]
        List<Tag> getTags();

        [OperationContract]
        void editTag(Tag s);

    }
}
