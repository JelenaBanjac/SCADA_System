﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WCFCommon
{
    [DataContract]
    [KnownType(typeof(DigitalInput))]
    [KnownType(typeof(DigitalOutput))]
    public abstract class Digital : Tag
    {
        public Digital() : base() { }

        public Digital(string tagId, string description, string IOaddress) 
            : base(tagId, description, IOaddress) { }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
